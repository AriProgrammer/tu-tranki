import * as React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

function Feed() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Feed!</Text>
        </View>
    );
}

function Profile() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Profile!</Text>
        </View>
    );
}

function Notifications() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Notifications!</Text>
        </View>
    );
}
const Tab = createBottomTabNavigator();

function StackScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ title: 'My home' }}
      />
    </Stack.Navigator>
  );
}

function MyTabs() {
    return (
        <Tab.Navigator
            initialRouteName="Feed"
            tabBarOptions={{
                activeTintColor: '#e91e63',
                style: {height: 140, borderTopColor: '#ff6b6c', borderTopWidth: 2}
            }}
        >
            <Tab.Screen
                name="Feed"
                component={Feed}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color, size }) => (
                        <Image
                            style={styles.theIcons}
                            source={require('./src/assets/icons/footer_reportar_incidente.png')}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Notifications"
                component={Notifications}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color, size }) => (
                        <View style={{backgroundColor: '#ff6b6c', borderRadius: 100/2, width: 80, height: 80, justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            style={{width: 55, height: 55}}
                            source={require('./src/assets/icons/footer_sirena.png')}
                        />
                        </View>
                    ),
                }}
            />
            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color, size }) => (
                        <Image
                            style={styles.theIcons}
                            source={require('./src/assets/icons/footer_servicios_emergencias.png')}
                        />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

const styles = StyleSheet.create({
    theIcons: {
        width: 40, height: 40
    },
});

export default function App() {
    return (
        <NavigationContainer>
          <StackScreen/>
            <MyTabs />
        </NavigationContainer>
    );
}
